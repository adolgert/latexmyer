import os
import sys
import logging
import argparse
try:
    import colorama
    have_color=True
except:
    have_color=False

try:
    import diff_match_patch
except ImportError:
    print('Need google-diff-patch-match library: '+\
        'http://code.google.com/p/google-diff-match-patch/')
    sys.exit(3)

logger=logging.getLogger("dmp")



def color_print(diffs):
    line_cnt=0
    color={-1 : colorama.Fore.RED, 0 : colorama.Fore.BLACK,
            1 : colorama.Fore.GREEN }
    for delta, value in diffs:
        sys.stdout.write(color[delta]+value)
        line_cnt+=value.count('\n')


def by_line(diffs):
    line_cnt={-1 : 0, 0 : 0, 1 : 0}
    prefixes={-1 : '<<<', 0 : '', 1 : '>>>'}
    if have_color:
        color={-1 : colorama.Fore.RED, 0 : colorama.Fore.BLACK,
                1 : colorama.Fore.GREEN }
    else:
        color={-1 : '', 0 : '', 1 : ''}
        
    for delta, value in diffs:
        cnt=(line_cnt[0]+line_cnt[-1], line_cnt[0]+line_cnt[1])
        if delta is not 0 and value not in '\n\t ':
            sys.stdout.write('{0}{1}{2},{3}{4}{5}{6}'.format(color[delta],
                prefixes[delta], cnt[0], cnt[1], os.linesep, value, os.linesep))
        line_cnt[delta]+=value.count('\n')



if __name__ == '__main__':
    parser=argparse.ArgumentParser(description="""Compare two latex files.
Basic usage is <program> file1 file2
For git, the format is <program> -f filename partial_hash1 partial_hash2
""")
    parser.add_argument('filenames', metavar='F', type=str,
            nargs='+', help='two filenames')
    if have_color:
        parser.add_argument('--sparse', help="Just print diffs.",
                action="store_true")
    parser.add_argument('-r', '--repo', help="Directory with a git repo. "+\
        "If omitted, the current directory is used.")
    parser.add_argument('-f', '--file', type=str,
            help="If you specify a filename, then"+\
            " the two free arguments should be hashes of the file.")
    args=parser.parse_args()

    logging.basicConfig(level=logging.INFO)
    logger.debug(args.filenames)

    texts=list()

    if args.file:
        if args.repo:
            repo_dir=args.repo
        else:
            repo_dir=os.getcwd()
        if len(args.filenames) != 2:
            print("Need two hashes of the file to compare.")
        import getgit
        text_dict=getgit.get(args.file, repo_dir, args.filenames)
        for by_hash in args.filenames:
            texts.append(text_dict[by_hash])

    else:
        if len(args.filenames)<2:
            print("Takes two filenames on the command line.")
            sys.exit(1)
        if len(args.filenames)>2:
            print("Don't know what to do with more than two filenames.")
        fnf=False
        for ef in args.filenames:
            if not os.path.exists(ef):
                fnf=True
                logger.error("Could not find {0}".format(ef))
        if fnf:
            sys.exit(2)
        for f in args.filenames:
            texts.append(''.join(open(f).readlines()))

    dmp=diff_match_patch.diff_match_patch()

    try:
        diffs=dmp.diff_main(texts[0], texts[1])
    except NameError as ne:
        if 'unichr' in ne.args[0]:
            logger.error('This script uses a library that doesn\'t have '+\
                'Python 3 support.')
            sys.exit(4)
        else:
            raise
    dmp.diff_cleanupSemantic(diffs)

    if have_color:
        colorama.init()
        if args.sparse:
            by_line(diffs)
        else:
            color_print(diffs)
    else:
        by_line(diffs)
