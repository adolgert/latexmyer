import logging
import StringIO
logger=logging.getLogger('getgit')
try:
    import git
except:
    logger.error('In order to work with git, this script needs'+\
        ' GitPython from https://gitorious.org/git-python.')
    raise


def get(filename, repo_dir, hash_list):
    repo=git.Repo(repo_dir)
    commits=dict()
    for any_comm in repo.iter_commits():
        for partial in hash_list:
            if partial == any_comm.hexsha[:len(partial)]:
                if not commits.has_key(partial):
                    commits[partial]=any_comm
                else:
                    raise RuntimeError(
                            'Two commit hashes match {0}'.format(partial))
    if len(commits)<2:
        logger.error('Could not find both hashes. Found {0}.'.format(
                commits.keys()))

    text=dict()
    for partial, commit in commits.items():
        for b in commit.tree.blobs:
            if b.name==filename:
                wo=StringIO.StringIO()
                b.stream_data(wo)
                text[partial]=wo.getvalue()

    return text
