LatexMyer - Latex Diff with Myer's Algorithm
=================================================

This computes a diff between two files using Myer's algorithm.
The main problem we have with diffs of Latex files is that some of our
editors reflow paragraphs, so this trivial program ignores paragraph marks
when it displays the diffs. It does not try to show only visual changes or
include changes for latex documents composed of multiple files, as
does [Latexdiff](http://www.ctan.org/pkg/latexdiff).

Depends on these Python modules:

* [GitPython](https://gitorious.org/git-python) - Needed only if you use git.

* [google-diff-match-patch](http://code.google.com/p/google-diff-match-patch/) -
  The file for this is included with the source.

* [colorama](https://pypi.python.org/pypi/colorama) - Optional, but much prettier.

License: Public domain, because I'm paid (ultimately) by the government, and
those are the rules.

Plans: If this works well enough, I can make a C++ version.
